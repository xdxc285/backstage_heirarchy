# Backstage Hierarchy Repo

### Domains, Sub-Domains, Commerce Channels
This repo houses all of the Backstage software-config.yaml along with the mkdocs mkdocs.yaml for each domain and system.

These domains and systems will show how each software component fits into the grainger hierachy.  This also allows for documentation to be added for each domain or system.


### Teams and Users
Another part of this hierachy is the user and team structure.  This allows for software compoents and documentation to be assigned to a given owner/team.  Keeping this up to date will allow for easier converstaions during issues with a clear line of ownership.

The creation of Teams and Users is currently a manual process but will soon be automated.
